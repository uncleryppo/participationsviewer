package de.ithappens.run2.participationsviewer;

import com.dlsc.workbenchfx.Workbench;

import de.ithappens.run2.participationsviewer.module.ParticipationsModule;
import javafx.fxml.FXML;

public class PvController {

	@FXML
	private Workbench workbench;

	ParticipationsModule pm;

	@FXML
	private void initialize() {
		pm = new ParticipationsModule(this);
		workbench.getModules().add(pm);
	}

	public void openDefaultModule() {
		workbench.openModule(pm);
		pm.loadYearParticipations("2019");
	}

}
