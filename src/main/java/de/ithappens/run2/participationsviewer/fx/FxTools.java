package de.ithappens.run2.participationsviewer.fx;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.image.Image;

/**
 * Copyright: 2018 Organisation: IT-Happens.de
 * 
 * @author riddler
 */
public class FxTools {

	private final static Logger log = LogManager.getLogger(FxTools.class);
	private final static SimpleDateFormat DF = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");

	private final static String BUNDLE_NAME = "de.ithappens.run2.participationsviewer";
	private final static ResourceBundle RB = ResourceBundle.getBundle(BUNDLE_NAME);

	public final static String RUN2 = get("RUN2");
	public final static String PARTICIPATIONS = get("PARTICIPATIONS");
	public final static String LOAD = get("LOAD");
	public final static String NO_PARTICIPATIONS_YET = get("NO_PARTICIPATIONS_YET");

	public static String APP_TITLE() {
		return StringUtils.join(new String[] { RUN2, PARTICIPATIONS }, " ");
	}

	@SuppressWarnings("finally")
	private static String get(String key) {
		String value = null;
		try {
			value = RB.getString(key);
		} catch (Exception ex) {
			ex.printStackTrace();
			value = "<TEXT_ERROR>";
		} finally {
			if (value == null || value.equals("")) {
				value = key;
			}
			return StringEscapeUtils.unescapeJava(value);
		}
	}

	private final static String FX_LOCATION = "de/ithappens/run2/participationsviewer/fx/";

	public static FXMLLoader getFXMLLoader(String fxmlFilename) {
		FXMLLoader loader = null;
		try {
			ClassLoader classLoader = FxTools.class.getClassLoader();
			String bundleFilename = StringUtils.replace(BUNDLE_NAME, ".", "/") + ".properties";
			log.debug("bundleFilename: " + bundleFilename);
			loader = new FXMLLoader(classLoader.getResource(FX_LOCATION + fxmlFilename),
					new PropertyResourceBundle(classLoader.getResource(bundleFilename).openStream()));
		} catch (IOException iox) {
			iox.printStackTrace();
			log.error(iox);
		}
		return loader;
	}

	public static Parent loadFxml(FXMLLoader loader) {
		Parent parent = null;
		try {
			parent = (Parent) loader.load();
		} catch (IOException iox) {
			iox.printStackTrace();
			log.error(iox);
		}
		return parent;
	}
	
	public final static Image APP_ICON = getAppIcon("application_icon.png");
	public final static Image IMG__PARTICIPATIONS = getAppIcon("clicknrun.png");
	public final static Image IMG__2019_LOGO = getAppIcon("Logo_14.Lauf_2019_Stand_26.01.2019.jpg");

	@SuppressWarnings("finally")
	private static Image getAppIcon(String imageFilename) {
		Image theImage = null;
		ClassLoader classLoader = FxTools.class.getClassLoader();
		try {
			theImage = new Image(classLoader.getResource(FX_LOCATION + imageFilename).openStream());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			return theImage;
		}
	}

}
