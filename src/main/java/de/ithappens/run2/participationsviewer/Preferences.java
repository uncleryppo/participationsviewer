package de.ithappens.run2.participationsviewer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import de.ithappens.commons.usercontext.EnvironmentPreference;

public class Preferences extends EnvironmentPreference {
	
	private static final String CONFIG_FILE_NAME = "participationsviewer.config";
	private final Properties configFile = new Properties();
	
	public Preferences() throws FileNotFoundException, IOException {
		configFile.load(new FileReader(CONFIG_FILE_NAME));
	}
	
	private String getConfigString(String key) {
		return configFile.getProperty(key);
	}
	
	@Override
	public CONTEXT getContext() {
		return CONTEXT.USER;
	}
	
	private final static String VALID_YEARS = "valid-years";
	private final static String PARTICIPATIONS_SOURCE_URL = "participations-source-url";
	private final static String PARTICIPATIONS_SOURCE_FILE_PREFIX = "participations-source-file-prefix";
	private final static String PARTICIPATIONS_SOURCE_FILE_EXTENSION = "participations-source-file-extension";
	
	public String[] getValidYears() {
		String validYearsArrayString = getConfigString(VALID_YEARS);
		return StringUtils.isNotEmpty(validYearsArrayString) ? StringUtils.split(validYearsArrayString, ",") : null;
	}
	
	public String getParticipationsSourceUrl() {
		return getConfigString(PARTICIPATIONS_SOURCE_URL);
	}
	
	public String getParticipationsSourceFilePrefix() {
		return getConfigString(PARTICIPATIONS_SOURCE_FILE_PREFIX);
	}
	
	public String getParticipationsSourceFileExtension() {
		return getConfigString(PARTICIPATIONS_SOURCE_FILE_EXTENSION);
	}


}
