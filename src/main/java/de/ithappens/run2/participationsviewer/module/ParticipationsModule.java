package de.ithappens.run2.participationsviewer.module;

import de.ithappens.run2.participationsviewer.PvController;
import de.ithappens.run2.participationsviewer.fx.FxTools;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;

public class ParticipationsModule extends ExtWorkbenchModule {

	private PmController control = null;

	public ParticipationsModule(PvController mainController) {
		super(mainController, FxTools.PARTICIPATIONS, MaterialDesignIcon.RUN, "participationsviewer.fxml");
	}

	@Override
	public void initOnce_hook() {
		control = getLoader().getController();
		control.buildUi(this);
	}

	public void loadYearParticipations(String year) {
		control.loadYearParticipations(year);
	}

}
