package de.ithappens.run2.participationsviewer.module;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dlsc.workbenchfx.Workbench;
import com.dlsc.workbenchfx.model.WorkbenchModule;

import de.ithappens.run2.participationsviewer.PvController;
import de.ithappens.run2.participationsviewer.fx.FxTools;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

public abstract class ExtWorkbenchModule extends WorkbenchModule {

	private final PvController mainController;
	private Node ui = null;
	private FXMLLoader loader;
	private Logger log = LogManager.getLogger();
	private String fxmlFilename;

	private boolean onceInitDone = false;

	public ExtWorkbenchModule(PvController mainController, String name, MaterialDesignIcon icon, String fxmlFilename) {
		super(name, icon);
		this.mainController = mainController;
		this.fxmlFilename = fxmlFilename;
	}

	public Logger log() {
		return log;
	}

	public PvController getMainController() {
		return mainController;
	}

	public void initOnce_hook() {
	}

	public void init_hook() {
	}

	@Override
	public void init(Workbench workbench) {
		super.init(workbench);
		if (!onceInitDone) {
			loader = FxTools.getFXMLLoader(fxmlFilename);
			ui = FxTools.loadFxml(loader);
			initOnce_hook();
			onceInitDone = true;
		}
		init_hook();
	}

	public FXMLLoader getLoader() {
		return loader;
	}

	@Override
	public Node activate() {
		return ui;
	}

}
