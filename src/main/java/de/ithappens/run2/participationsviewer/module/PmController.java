package de.ithappens.run2.participationsviewer.module;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.json.XML;

import com.dlsc.workbenchfx.view.controls.ToolbarItem;
import com.google.gson.Gson;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.run2.participationsviewer.Preferences;
import de.ithappens.run2.participationsviewer.fx.FxTools;
import de.ithappens.run2.participationsviewer.model.Participation;
import de.ithappens.run2.participationsviewer.model.ParticipationsContainer;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

public class PmController {

	private ParticipationsModule pmModule;
	private ParticipationsContainer pConti;

	@FXML
	private TableView<Participation> participations_tbv;
	@FXML
	private ScrollPane participations_scp;
	@FXML
	private TableColumn<Participation, String> gender_tvc, yearOfBirth_tvc, name_tvc, track_tvc, discipline_tvc;

	private Preferences PREFS = UserContext.getInstance().getEnvironment(Preferences.class);
	
	private final String[] validYears = PREFS.getValidYears();
	private final String sourceUrl = PREFS.getParticipationsSourceUrl();

	private final Label currentYear_lbl = new Label();

	private void createAndAddYearButtons(String[] years) {
		for (String year : years) {
			Button b = new Button(year);
			b.setOnMouseReleased((e) -> {
				actionLoad(year);
			});
			pmModule.getToolbarControlsLeft().add(new ToolbarItem(b));
		}
	}

	public void buildUi(ParticipationsModule pm) {
		pmModule = pm;

		pmModule.getToolbarControlsLeft().clear();
		pmModule.getToolbarControlsRight().clear();
		pmModule.getToolbarControlsRight().add(new ToolbarItem(currentYear_lbl));
		createAndAddYearButtons(validYears);

		gender_tvc.setCellValueFactory(
				cellData -> cellData.getValue() != null ? new SimpleStringProperty(cellData.getValue().getGender())
						: null);
		yearOfBirth_tvc.setCellValueFactory(
				cellData -> cellData.getValue() != null ? new SimpleStringProperty(cellData.getValue().calcAgeClass(new Integer(currentYear_lbl.getText())))
						: null);
		name_tvc.setCellValueFactory(cellData -> cellData.getValue() != null
				? new SimpleStringProperty(cellData.getValue().getParticipantName())
				: null);
		track_tvc.setCellValueFactory(
				cellData -> cellData.getValue() != null ? new SimpleStringProperty(cellData.getValue().getTrack())
						: null);
		discipline_tvc.setCellValueFactory(
				cellData -> cellData.getValue() != null ? new SimpleStringProperty(cellData.getValue().getDiscipline())
						: null);
		participations_tbv.setPlaceholder(new Label(FxTools.NO_PARTICIPATIONS_YET));
		participations_tbv.setStyle("-fx-background-color: transparent; "
				+ "-fx-base: rgba(255, 255, 255, 0.6); ");
		
		participations_tbv.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		participations_scp.setFitToHeight(true);
		participations_scp.setFitToWidth(true);
		
//		participations_tbv.setVisible(false);
		
	}

	public void loadYearParticipations(String year) {
		for (String validYear : validYears) {
			if (validYear.equals(year)) {
				actionLoad(year);
				break;
			}
		}
	}
	
	private String buildSourceFileUrl(String year) {
		return sourceUrl + PREFS.getParticipationsSourceFilePrefix() + year + PREFS.getParticipationsSourceFileExtension();
	}

	private void actionLoad(String year) {
		try {
			participations_tbv.getItems().clear();
			currentYear_lbl.setText(year);
			String fileContent = loadSourceFileFromServer(
					buildSourceFileUrl(year));
			pConti = parse(fileContent);
			participations_tbv.getItems().addAll(pConti.getParticipations());
			if ("2019".equals(year)) {
				BackgroundImage big = new BackgroundImage(FxTools.IMG__2019_LOGO, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                        BackgroundPosition.DEFAULT,
                        new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, true, true, true, true));
				participations_tbv.setBackground(new Background(big));
			} else {
				participations_tbv.setBackground(null);
			}
		} catch (AuthenticationException | KeyManagementException | NoSuchAlgorithmException | KeyStoreException
				| IOException e) {
			e.printStackTrace();
		}
	}
	
	private ParticipationsContainer parse(String xmlFileContent) {
		ParticipationsContainer container = null;
		if (StringUtils.isNotEmpty(xmlFileContent)) {
			JSONObject xmlToJsonObj = XML.toJSONObject(xmlFileContent).getJSONObject("filledin");
			Participation[] participations = new Gson().fromJson(xmlToJsonObj.getJSONArray("submission").toString(),
					Participation[].class);
			System.out.println(xmlFileContent);
			container = new ParticipationsContainer();
			container.getParticipations().addAll(Arrays.asList(participations));
		}
		return container;
	}

	public String loadSourceFileFromServer(String url) throws IOException, AuthenticationException,
			KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		String fileContent = null;
		if (url != null) {
			try (CloseableHttpClient client = HttpClients.createDefault()) {
				String urlData = "";
				URL urlObj = new URL(url);
				URLConnection conn = urlObj.openConnection();
				try (BufferedReader reader = new BufferedReader(
						new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
					urlData = reader.lines().collect(Collectors.joining("\n"));
				}
				fileContent = urlData;

			}
		}
		return fileContent;
	}

}
