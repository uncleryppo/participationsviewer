package de.ithappens.run2.participationsviewer;

import com.jpro.webapi.JProApplication;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.run2.participationsviewer.fx.FxTools;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ParticipationsViewer extends JProApplication {

	public static void main(String args[]) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			UserContext.getInstance().addEnvironment(new Preferences());
			getWebAPI();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("workbench.fxml"));
			Parent root = loader.load();
			PvController control = loader.getController();
			primaryStage.setTitle(FxTools.APP_TITLE());
			primaryStage.getIcons().add(FxTools.APP_ICON);
			Scene myScene = new Scene(root);
			primaryStage.setScene(myScene);
			primaryStage.show();

			control.openDefaultModule();

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

}
