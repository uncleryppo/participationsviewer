package de.ithappens.run2.participationsviewer.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.google.gson.annotations.SerializedName;

public class Participation {

	@SerializedName("date")
	private String dateOfSubmission;
	@SerializedName("time")
	private String timeOfSubmission;

	@SerializedName("field")
	private Field[] attributes;

	public String getParticipantName() {
		return getAttributeValue("Teilnehmer");
	}

	public String getEMail() {
		return getAttributeValue("EMail");
	}

	public String getAddress() {
		return getAttributeValue("Adresse");
	}

	public String getPhoneNumber() {
		return getAttributeValue("Telefonnummer");
	}

	public String getBirthyear() {
		return getAttributeValue("Jahrgang");
	}
	
	public String calcAgeClass(Integer currentYear) {
		String ageClass = null;
		String birthyearString = getBirthyear();
		if (currentYear != null && NumberUtils.isNumber(birthyearString)) {
			Integer birthyear = new Integer(birthyearString);
			int age = currentYear - birthyear;
			if (age >= 95) {
				ageClass = "MW 95";
			} else if (age >= 90) {
				ageClass = "MW 90";
			} else if (age >= 85) {
				ageClass = "MW 85";
			} else if (age >= 80) {
				ageClass = "MW 80";
			} else if (age >= 75) {
				ageClass = "MW 75";
			} else if (age >= 70) {
				ageClass = "MW 70";
			} else if (age >= 65) {
				ageClass = "MW 65";
			} else if (age >= 60) {
				ageClass = "MW 60";
			} else if (age >= 55) {
				ageClass = "MW 55";
			} else if (age >= 50) {
				ageClass = "MW 50";
			} else if (age >= 45) {
				ageClass = "MW 45";
			} else if (age >= 40) {
				ageClass = "MW 40";
			} else if (age >= 35) {
				ageClass = "MW 35";
			} else if (age >= 30) {
				ageClass = "MW 30";
			} else if (age >= 23) {
				ageClass = "M/F";
			} else if (age >= 20) {
				ageClass = "J U23";
			} else if (age >= 19) {
				ageClass = "J U20";
			} else if (age >= 17) {
				ageClass = "J U18";
			} else if (age >= 15) {
				ageClass = "J U16";
			} else if (age >= 13) {
				ageClass = "J U14";
			} else if (age >= 11) {
				ageClass = "J U12";
			} else if (age >= 9) {
				ageClass = "J U10";
			} else if (age >= 7) {
				ageClass = "J U8";
			} else {
				ageClass = "Bambini";
			}
		}
		return ageClass;
	}

	public String getGender() {
		return getAttributeValue("Geschlecht");
	}

	public String getDiscipline() {
		return getAttributeValue("Sportart");
	}

	public String getTrackAndCharge() {
		return getAttributeValue("StreckeUndStartgeld");
	}
	
	public String getTrack() {
		String trackAndCharge = getTrackAndCharge();
		if (StringUtils.isNotEmpty(trackAndCharge)) {
			return StringUtils.substringBeforeLast(trackAndCharge, " /");
		} else {
			return null;
		}
	}

	public String getTeam() {
		return getAttributeValue("Team");
	}

	public String getBeneficial() {
		return getAttributeValue("Spende");
	}

	public String getAnnotations() {
		return getAttributeValue("Bemerkungen");
	}

	private String getAttributeValue(String name) {
		if (name != null && attributes != null) {
			for (Field attribute : attributes) {
				if (name.equals(attribute.getName())) {
					return attribute.getContent();
				}
			}
		}
		return null;
	}

	public Field[] getAttributes() {
		return attributes;
	}

	public String getDateOfSubmission() {
		return dateOfSubmission;
	}

	public void setDateOfSubmission(String dateOfSubmission) {
		this.dateOfSubmission = dateOfSubmission;
	}

	public String getTimeOfSubmission() {
		return timeOfSubmission;
	}

	public void setTimeOfSubmission(String timeOfSubmission) {
		this.timeOfSubmission = timeOfSubmission;
	}

	public class Field {

		private String name;
		private String content;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

	}

}
