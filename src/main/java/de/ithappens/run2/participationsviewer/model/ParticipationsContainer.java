package de.ithappens.run2.participationsviewer.model;

import java.util.ArrayList;
import java.util.List;

public class ParticipationsContainer {
	
	private List<Participation> participations = new ArrayList<>();
	
	public List<Participation> getParticipations() {
		return participations;
	}
	
}
