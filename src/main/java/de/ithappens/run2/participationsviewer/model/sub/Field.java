package de.ithappens.run2.participationsviewer.model.sub;

import com.google.gson.annotations.SerializedName;

public class Field {
	
	@SerializedName("@name")
	private String name;
	private String content;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
